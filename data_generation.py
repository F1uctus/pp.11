import random
from datetime import timedelta, datetime

import psycopg2 as pg
from faker import Faker
from faker.providers import internet, phone_number, person

fake = Faker(['ru_RU'])
fake.add_provider(phone_number)
fake.add_provider(internet)
fake.add_provider(person)

newline = '\n'
newline_escaped = '\\n'
connection = pg.connect("dbname=gkaz user=az_admin password=???")
cursor = connection.cursor()

order_types = [
    "Компьютер под офисные задачи на GNU/Linux",
    "Компьютер с платформой 1С:Предприятие на Linux",
    "Сервер виртуализации и сетевая инфраструктура на Linux",
    "Индивидуальное решение на GNU/Linux",
    "Обучение свободному программному обеспечению",
    "Дистрибутив GNU/Linux под собственным брендом",
    "Перестройка бизнес-процесса под работу во FreeCAD"
]


def insert_order_types():
    for i, order_type in enumerate(order_types):
        cursor.execute(f"""
        insert into order_types values ({i}, '{order_type}')
        """)


def insert_customers(count):
    for i in range(count):
        cursor.execute(f"""
        insert into customers values (
            {i},
            '{random_first_last_name()}',
            '{fake.address()}',
            '{fake.phone_number()}',
            '{fake.email()}'
        )""")


def random_first_last_name() -> str:
    return fake.first_name_male() + ' ' + fake.last_name_male() \
        if bool(random.getrandbits(1)) \
        else fake.first_name_female() + ' ' + fake.last_name_female()


def random_date(start, end) -> datetime:
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)


def generate_order_timestamps() -> (datetime, datetime):
    # 2 даты в диапазоне [сегодня - 5 недель, сегодня + 5 недель]
    now = datetime.now()
    dt_start = random_date(
        now - timedelta(weeks=5), now
    ).replace(minute=0, second=0, microsecond=0, tzinfo=None)
    dt_end = random_date(
        now, now + timedelta(weeks=5)
    ).replace(minute=0, second=0, microsecond=0, tzinfo=None)
    return dt_start, dt_end


def insert_orders(count, customer_ids_count, order_ids_count):
    for i in range(count):
        price = random.random() * 45000 + 5000
        quantity = random.randint(1, 5)
        created_at, complete_at = generate_order_timestamps()
        cursor.execute(fr"""
        insert into orders values (
            default,
            {random.randint(0, customer_ids_count - 1)},
            {random.randint(0, order_ids_count - 1)},
            e'{fake.text().replace(newline, newline_escaped)}',
            '{price:.2f}',
            null,
            {quantity},
            '{created_at.isoformat(sep=' ')}'::timestamp,
            '{complete_at.isoformat(sep=' ')}'::timestamp
        )""")


insert_order_types()
insert_customers(3000)
insert_orders(3000, 3000, len(order_types))

connection.commit()

cursor.close()
connection.close()
