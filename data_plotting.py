from collections import Counter

import pandas
import psycopg2 as pg
import seaborn
from matplotlib import pyplot


def show_date_series_bar_plot(data, x_title, y_title):
    x, y = data.keys(), data.values()

    df = pandas.DataFrame({x_title: x, y_title: y}, columns=[x_title, y_title])

    _, ax = pyplot.subplots(figsize=(len(x) * 0.3, 6))
    seaborn.barplot(x=x_title, y=y_title, data=df, ax=ax)

    y_min, y_max = min(y), max(y)
    y_margin = 4

    ax.set_yticks(range(0, y_max + y_margin, 2))
    ax.set(ylim=(y_min - y_margin, y_max + y_margin))
    ax.yaxis.grid(b=True, which='major', linewidth=0.5)

    x_dates = df[x_title].dt.strftime('%Y-%m-%d')
    ax.set_xticklabels(labels=x_dates, rotation=45, ha='right')

    pyplot.show()


def plot_order_creation_dates(cur):
    cur.execute("select created_at from orders order by created_at asc")

    data = Counter(row[0].replace(hour=0, minute=0) for row in cur.fetchall())

    # This fixes distribution error in our test cases
    del data[max(data.keys())]

    show_date_series_bar_plot(
        data,
        x_title='Последние 5 недель',
        y_title='Количество зарегистрированных заказов'
    )


def plot_order_completing_dates(cur):
    cur.execute("select complete_at from orders order by complete_at asc")

    data = Counter(row[0].replace(hour=0, minute=0) for row in cur.fetchall())

    # This fixes distribution error in our test cases
    del data[max(data.keys())]

    show_date_series_bar_plot(
        data,
        x_title='Следующие 5 недель',
        y_title='Количество заказов на завершение'
    )


def plot_customers_map(cursor):
    pass


if __name__ == "__main__":
    connection = pg.connect("dbname=gkaz user=az_admin password=???")
    cursor = connection.cursor()

    #plot_order_creation_dates(cursor)
    plot_order_completing_dates(cursor)

    cursor.close()
    connection.close()
